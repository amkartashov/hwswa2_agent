﻿using sys = System;
using io = System.IO;
using pipes = System.IO.Pipes;
using text = System.Text;
using threading = System.Threading;
using svc_process = System.ServiceProcess;

using Console = System.Console;
using Thread = System.Threading.Thread;
using ManualResetEvent = System.Threading.ManualResetEvent;
using ThreadStart = System.Threading.ThreadStart;
using TimeSpan = System.TimeSpan;
using Stream = System.IO.Stream;
using StreamReader = System.IO.StreamReader;
using StreamWriter = System.IO.StreamWriter;
using EventLog = System.Diagnostics.EventLog;
using sList = System.Collections.Generic.List<string>;
using MethodInfo = System.Reflection.MethodInfo;
using ServiceBase = System.ServiceProcess.ServiceBase;

/* ToDo

service

pipe security

commands:
stop
exit
listen
send
receive
exec
ni_exec
 
 
*/

static class Program
{
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    static void Main()
    {
        svc_process.ServiceBase.Run(new PipeServer());
    }
}

public class PipeServer : ServiceBase
{
    private static int numThreads = 25;
    private static bool stop = false;
    private static Thread[] servers = new Thread[numThreads];
    private static string pipename = "hwswa2_agent";
    private static bool DEBUG = true;
    private static sList commands = new sList();
    protected Thread m_thread;
    protected ManualResetEvent m_shutdownEvent;
    protected TimeSpan m_delay;

    public PipeServer()
    {
        this.ServiceName = "hwswa2_agent";
        this.CanStop = true;
        this.CanPauseAndContinue = false;
        this.AutoLog = false;
        this.m_delay = new TimeSpan(0, 0, 0, 10, 0);
    }


    protected override void OnStart(string[] args)
    {
        // create our threadstart object to wrap our delegate method
        ThreadStart ts = new ThreadStart(this.ServiceMain);

        // create the manual reset event and
        // set it to an initial state of unsignaled
        m_shutdownEvent = new ManualResetEvent(false);

        // create the worker thread
        m_thread = new Thread(ts);

        // go ahead and start the worker thread
        m_thread.Start();

        // call the base class so it has a chance
        // to perform any work it needs to
        Debug("Service started");
        base.OnStart(args);
    }

    /// <SUMMARY>
    /// Stop this service.
    /// </SUMMARY>
    protected override void OnStop()
    {
        // signal the event to shutdown
        m_shutdownEvent.Set();

        // wait for the thread to stop giving it 10 seconds
        m_thread.Join(10000);

        // call the base class
        Debug("Service stopped");
        base.OnStop();
    }

    protected void ServiceMain()
    {
        Debug("Waiting for client connect...\n");
        // init commands list
        foreach (MethodInfo method in typeof(PipeServer).GetMethods())
        {
            string mname = method.Name;
            if (mname.StartsWith("cmd_"))
            {
                commands.Add(mname.Substring("cmd_".Length));
            }
        }
        for (int i = 0; i < numThreads; i++)
        {
            servers[i] = new Thread(ServerThread);
            servers[i].Start();
        }
        Thread.Sleep(250);
        while (! (stop || m_shutdownEvent.WaitOne( m_delay, true )))
        {
            for (int i = 0; i < numThreads; i++)
            {
                if (! servers[i].IsAlive)
                {
                    Debug("Server thread[" + servers[i].ManagedThreadId + "] finished.");
                    servers[i] = null;
                    servers[i] = new Thread(ServerThread);
                    servers[i].Start();
                }
            }
        }
        if (stop)
        {
            Debug("Command stop received, exiting.");
            base.Stop();
        }
    }

    private static void ServerThread(object data)
    {
        pipes.NamedPipeServerStream pipeServer = new pipes.NamedPipeServerStream(
            pipename, 
            pipes.PipeDirection.InOut, 
            numThreads, 
            pipes.PipeTransmissionMode.Byte, 
            pipes.PipeOptions.Asynchronous | pipes.PipeOptions.WriteThrough);

        int threadId = Thread.CurrentThread.ManagedThreadId;

        pipeServer.BeginWaitForConnection(new sys.AsyncCallback(WaitForConnectionCallBack), pipeServer);

        while (!stop)
        {
            if (pipeServer.IsConnected)
            {
                Debug("Client connected on thread[" + threadId + "].");
                try
                {
                    // Read the request from the client. Once the client has 
                    // written to the pipe its security token will be available.

                    bool exit = false;
                    StreamReader sr = new StreamReader(pipeServer);
                    StreamWriter sw = new StreamWriter(pipeServer);
                    sw.AutoFlush = true;

                    // Verify our identity to the connected client using a 
                    // string that the client anticipates.

                    sw.WriteLine("HWSWA2 Windows agent, available commands: help, exit, stop, " + string.Join(", ", commands.ToArray()));
                    string command = sr.ReadLine();

                    while (command != null)
                    {
                        Debug("Received command: " + command +" on thread[" + threadId +"] as user: " + pipeServer.GetImpersonationUserName());
                        string cmd = "";
                        string[] args = {};
                        if (command.IndexOf(' ') != -1)
                        {
                            string[] cmdargs = command.Split(new char[] { ' ' }, 2);
                            cmd = cmdargs[0];
                            args = cmdargs[1].Split(new char[] { ' ' });
                        }
                        else
                        {
                            cmd = command;
                        }



                        switch (cmd)
                        {
                            case "stop":
                                stop = true;
                                exit = true;
                                sw.WriteLine("accepted_ok stopping agent");
                                break;
                            case "exit":
                                exit = true;
                                sw.WriteLine("accepted_ok closing connection");
                                break;
                            default:
                                sw.WriteLine("accepted_notok no such command " + cmd);
                                break;
                        }
                        if (exit || stop) break;
                        command = sr.ReadLine();
                    }
                }
                // Catch the IOException that is raised if the pipe is broken 
                // or disconnected. 
                catch (io.IOException e)
                {
                    Debug("ERROR: " + e.Message);
                }
                pipeServer.Close();
                Debug("Stopping thread[" + threadId + "] after closing connection.");
                break;
            }
            Thread.Sleep(250);
        }
        if (stop) Debug("Stopping thread[" + threadId + "] because stop received.");
    }

    private static void WaitForConnectionCallBack(sys.IAsyncResult iar)
    {
        try
        {
            // Get the pipe
            pipes.NamedPipeServerStream pipeServer = (pipes.NamedPipeServerStream)iar.AsyncState;
            // End waiting for the connection
            pipeServer.EndWaitForConnection(iar);
        }
        catch
        {
            return;
        }
    }

    private static void Debug(string message) {
        if (DEBUG)
        {
            string source = "hwswa2_agent";
            string log = "Application";

            if (!EventLog.SourceExists(source))
                EventLog.CreateEventSource(source, log);

            EventLog.WriteEntry(source, message);
        }
    }

    public static string cmd_close(string proto, string address, string ports)
    {
        return "not implemented";
    }

    public static string cmd_closeall()
    {
        return "not implemented";
    }

    public static string cmd_listen(string proto, string address, string ports)
    {
        return "not implemented";
    }

    public static string cmd_receive(string proto, string address, string ports, string timeout = "1")
    {
        return "not implemented";
    }

    public static string cmd_exec(string cmd, string timeout = "10")
    {
        return "not implemented";
    }

}